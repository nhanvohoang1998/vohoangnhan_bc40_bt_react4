import logo from './logo.svg';
import './App.css';
import Redux_Mini from './Redux_Mini/Redux_Mini';

function App() {
  return (
    <div className="App">
        <Redux_Mini/>
    </div>
  );
}

export default App;
