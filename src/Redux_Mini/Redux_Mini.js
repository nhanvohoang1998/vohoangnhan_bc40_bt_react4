import React, { Component } from 'react'
import { connect } from 'react-redux'

class Redux_Mini extends Component {
  render() {
    console.log(this.props)
    return (
      <div>
        <button onClick={this.props.handleGiamSoLuong} className='btn btn-warning'>-</button>
        <span className='mx-4'>{this.props.SOLUONG}</span>
        <button onClick={this.props.handleTangSoLuong} className='btn btn-warning'>+</button>
      </div>
    )
  }
}

let mapStateToProps = (state) =>{
  return {
    SOLUONG: state.numberReducer.number
  }
}

let mapDispatchToProps = (dispatch) =>{
  return {
    handleTangSoLuong: ()=>{
      let action = {
        type: "TANGSOLUONG"
      }
      dispatch(action)
    },
    handleGiamSoLuong: ()=>{
      let action = {
        type: "GIAMSOLUONG",
        payload: 1,
      }
      dispatch(action)
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Redux_Mini)