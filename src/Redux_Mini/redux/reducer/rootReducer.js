import { numberReducer } from "./numberReducer";
import {combineReducers} from "redux";


export const rootReducer_ReduxMini = combineReducers({
    numberReducer,
})