let initialValue = {
    number:5,
}
export const numberReducer = (state = initialValue, action)=>{
    switch(action.type){
        case "TANGSOLUONG":{
            state.number++;
            return {...state}
        }

        case "GIAMSOLUONG":{
            state.number-=action.payload;
            return{...state};
        }

        default: {
            return state;
        }
    }
} 